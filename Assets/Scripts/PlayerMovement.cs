﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
public class PlayerMovement : MonoBehaviour {



    // These variables are for adjusting in the inspector how the object behaves 
    public float maxSpeed = 7;
    public float force = 8;
    public float jumpSpeed = 5;
    [SerializeField]
    private Camera cam;

    private Vector3 velocity = Vector3.zero;
    private Vector3 rotation = Vector3.zero;
    public float sensitivityX = 15F;
    public float sensitivityY = 15F;

    public float minimumX = -360F;
    public float maximumX = 360F;

    public float minimumY = -60F;
    public float maximumY = 60F;
    float rotationY = 0F;
    float rotationX = 0F;
    // These variables are there for use by the script and don't need to be edited
    private int state = 0;
    private bool grounded = false;
    private float jumpLimit = 0;
    private Rigidbody rb;
    CapsuleCollider col;
    float height;
    // Don't let the Physics Engine rotate this physics object so it doesn't fall over when running
    void Awake()
    {
        col = GetComponent<CapsuleCollider>();
        height = col.bounds.size.y;
        rb = GetComponent<Rigidbody>();
        rb.freezeRotation = true;
    }

    //// This part detects whether or not the object is grounded and stores it in a variable
    void OnCollisionEnter()
    {
        state++;
        if (state > 0)
        {
            grounded = true;
        }
    }

    //bool OnGround()
    //{
    //    if (Physics.Raycast(transform.position, Vector3.down, height / 2-0.1f))
    //        return true;
    //    return false;
    //}
    void OnCollisionExit()
    {
        state--;
        if (state < 1)
        {
            grounded = false;
            state = 0;
        }
    }


    public virtual bool jump
    {
        get
        {
            return Input.GetButtonDown("Jump");
        }
    }

    public virtual float horizontal
    {
        get
        {
            return Input.GetAxis("Horizontal") * force;
        }
    }
    public virtual float vertical
    {
        get
        {
            return Input.GetAxis("Vertical") * force;
        }
    }
    // This is called every physics frame
//    void FixedUpdate()
//    {
//       // grounded = OnGround();
//        print("grounded: " + grounded);

       
    


//}
    void Update()
    {
        //print(transform.rotation * Vector3.forward * vertical * Time.deltaTime);
        //print(transform.rotation);
        //print(vertical);
        //print(rb.velocity.magnitude); 
        print("V0: " + rb.velocity);

        // If the object is grounded and isn't moving at the max speed or higher apply force to move it
        if (rb.velocity.magnitude < maxSpeed)
        {
            rb.AddForce(transform.rotation * transform.forward* vertical*Time.deltaTime);
            rb.AddForce(transform.rotation * transform.right * horizontal * Time.deltaTime);

            //rb.position += transform.rotation *transform.right * horizontal*Time.deltaTime;
        }
        print("V1: " + rb.velocity);
        // This part is for jumping. I only let jump force be applied every 10 physics frames so
        // the player can't somehow get a huge velocity due to multiple jumps in a very short time
        if (jumpLimit < 10) jumpLimit++;

        if (jump && grounded && jumpLimit >= 10)
        {
            rb.velocity = rb.velocity + (Vector3.up * jumpSpeed);
            jumpLimit = 0;
        }
        print("V2: " + rb.velocity);

        Rotate();
      //  rb.AddForce(Vector3.up*100000);
        print("V3: " + rb.velocity);

    }
    void Rotate()
    {
        if (cam == null) { Debug.LogError("nocam attached"); return; }
        float rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;

        rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
        rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);
        transform.localEulerAngles = new Vector3(0, rotationX, 0);
        cam.transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
    }
}
