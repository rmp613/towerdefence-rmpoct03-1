﻿using UnityEngine;
using System.Collections;

public class BuildWeapon : BaseWeapon {

	private GameObject player;

	// Use this for initialization
	void Start () {
		GameObject[] gos = GameObject.FindObjectsOfType(typeof(GameObject)) as GameObject[]; //will return an array of all GameObjects in the scene
		foreach(GameObject go in gos)
		{
			if(go.layer == 8 && go.CompareTag("Player"))//go.layer=="LocalPlayer" && go.CompareTag("Player"))
			{
				player = go; 
			}
		}

		gunAnim.Play ("Draw");

		player.GetComponent<GridInteraction> ().EnableGridInteraction (true);
	}

	void Awake(){
		GameObject[] gos = GameObject.FindObjectsOfType(typeof(GameObject)) as GameObject[]; //will return an array of all GameObjects in the scene
		foreach(GameObject go in gos)
		{
			if(go.layer == 8 && go.CompareTag("Player"))//go.layer=="LocalPlayer" && go.CompareTag("Player"))
			{
				player = go; 
			}
		}

		player.GetComponent<GridInteraction> ().EnableGridInteraction (true);
		gunAnim.Play ("Draw");
	}


	void OnDisable(){
		player.GetComponent<GridInteraction> ().EnableGridInteraction (false);
	}
	
	// Update is called once per frame
	void Update () {

		if (player == null) {
			GameObject[] gos = GameObject.FindObjectsOfType(typeof(GameObject)) as GameObject[]; //will return an array of all GameObjects in the scene
			foreach(GameObject go in gos)
			{
				if(go.layer == 8 && go.CompareTag("Player"))//go.layer=="LocalPlayer" && go.CompareTag("Player"))
				{
					player = go; 
				}
			}
		}

		if (!player.GetComponent<GridInteraction> ().gridInteractionEnabled) {
			player.GetComponent<GridInteraction> ().EnableGridInteraction (true);
		}

		if ((Input.GetButtonDown ("Fire1") || Input.GetMouseButtonDown (0))) {
			gunAnim.Play ("Build");
			player.GetComponent<GridInteraction> ().BuildTower ();

		}


		if ((Input.GetButtonDown ("Fire2") || Input.GetMouseButtonDown (1))) {
			gunAnim.Play ("Destroy");
			player.GetComponent<GridInteraction> ().SellTower ();
		}

	
		if(Input.GetKeyDown(KeyCode.E)){
			player.GetComponent<GridInteraction> ().CycleTowerUp();
		}

		if(Input.GetKeyDown(KeyCode.Q)){
			player.GetComponent<GridInteraction> ().CycleTowerDown();
		}
	}
}
